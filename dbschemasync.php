<?php

/**
 * @param \Composer\Autoload\ClassLoader $autoloader
 * @param $config
 */
function addAutoload(\Composer\Autoload\ClassLoader $autoloader, $config)
{
    if ($config['autoload']) {
        if (!empty($config['autoload']['psr-0']) && is_array($config['autoload']['psr-0'])) {
            foreach ($config['autoload']['psr-0'] as $key => $value) {
                $prefix = is_int($key) ? 0 : $key;
                $autoloader->add($prefix, $value);
            }
        }
        if (!empty($config['autoload']['psr-4']) && is_array($config['autoload']['psr-4'])) {
            foreach ($config['autoload']['psr-4'] as $key => $value) {
                $prefix = is_int($key) ? 0 : $key;
                $autoloader->addPsr4($prefix, $value);
            }
        }
        if (!empty($config['autoload']['classmap']) && is_array($config['autoload']['classmap'])) {
            $autoloader->addClassMap($config['autoload']['classmap']);
        }
    }
}

/**
 * @param array $params
 * @param string $filter
 * @return \Doctrine\DBAL\Connection
 * @throws \Doctrine\DBAL\DBALException
 */
function getConnection(array $params, ?string $filter = '')
{
    $config = new \Doctrine\DBAL\Configuration();
    $connection = \Doctrine\DBAL\DriverManager::getConnection($params, $config);
    if (!empty($filter)) {
        $connection->getConfiguration()->setFilterSchemaAssetsExpression($filter);
    }
    return $connection;
}


/** @var \Composer\Autoload\ClassLoader $autoloader */
$autoloader = require 'vendor/autoload.php';

$config = require 'config.php';

addAutoload($autoloader, $config);

$apply = in_array('apply', $argv);

try {
    foreach ($config['types'] as $key => $value) {
        \Doctrine\DBAL\Types\Type::addType($key, $value);
    }

    $filter = !empty($config['schema_filter']) ? $config['schema_filter'] : '';

    $fromConnection = getConnection($config['from'], $filter);
    $toConnection = getConnection($config['to'], $filter);
    if (!empty($config['mapping_types'])) {
        foreach ($config['mapping_types'] as $k => $v) {
            $fromConnection->getDatabasePlatform()->registerDoctrineTypeMapping($k, $v);
            $toConnection->getDatabasePlatform()->registerDoctrineTypeMapping($k, $v);
        }
    }

    $fromSchema = $fromConnection->getSchemaManager()->createSchema();
    $toSchema = $toConnection->getSchemaManager()->createSchema();
    /**
     * Нет нужды синхронизировать последовательности,
     * при создании таблицы которая использует последовательность
     * Доктрина генерирует и запрос на создание соответствующей последовательности.
     * Из-за этого регулярно получается конфликт. Сначала последовательность создается
     * потому, что она уже есть на стороне источника, а потом она создается еще раз
     * при создании таблицы, которой еще не было.
     */
    foreach ([$fromSchema, $toSchema] as $schema) {
        /** @var Doctrine\DBAL\Schema\Schema $schema */
        $sequences = $schema->getSequences();
        foreach ($sequences as $sequence) {
            $schema->dropSequence($sequence->getFullQualifiedName($schema->getName()));
        }
    }

    $queries = $toSchema->getMigrateToSql($fromSchema, $toConnection->getDatabasePlatform());
    $apply && $toConnection->beginTransaction();
    try {
        foreach ($queries as $sql) {
            /*
             * Ниже заглушки под конкретные ситуации, пока нет времени искать корректные решения
             */
            if ($toConnection->getDatabasePlatform() instanceof \Doctrine\DBAL\Platforms\PostgreSqlPlatform) {
                $sql = str_replace("'now()'", 'CURRENT_TIMESTAMP', $sql);
            }
            if ($filter && preg_match('~^CREATE SCHEMA (.+)$~', $sql, $m)) {
                if (!preg_match($filter, $m[1])) {
                    continue;
                }
            }
            if ($apply) {
                echo '[' . date('Y-m-d H:i:s') . '] ';
                $toConnection->exec($sql);
            }
            echo $sql . ";\n";
        }
        $apply && $toConnection->commit();
    } catch (\Exception $exception) {
        $apply && $toConnection->rollback();
        throw $exception;
    }
} catch (\Exception $exception) {
    echo $exception . "\n";
}
